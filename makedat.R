# Make a dat file compatible with Precision Flight App
# TODO: write a geojson instead as import of that is now working Apr 2018
library(rgdal)
#gjson <- "PrecisionFlight.geojson"
#import <- readOGR("PrecisionFlight.geojson","PrecisionFlight",stringsAsFactors=FALSE)

layer <- "sensorNov1-2017"
gjson <- "geojson/sensorNov1-2017.geojson"
alt <- 30
speed <- 2
forwardlap <- 0.93
sidelap <- 0.93

import <- readOGR(gjson,layer,stringsAsFactors = FALSE)
poly <- import@polygons[[1]]@Polygons[[1]]@coords
export <- poly[1:nrow(poly)-1,c(2,1)]
txt <- paste("WPA",export[,1],export[,2],sep=" ")
header <- c("FlightPlannerVersion 23.0"
            ,"AltMode SelectedAlt"
            ,"SurveyAlt 20"
            ,"CaptureOverlapAlongTransect 0.93"
            ,"CaptureOverlapBetweenTransects 0.93"
            ,"SurveyAirSpeed 5"
            ,"SurveyRepetitions 1"
            ,"SurveyMode BOUNDED")
output <- c(header,txt)
writeLines(output, con=paste0("dat/",layer,".dat"))
